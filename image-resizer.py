import glob
from pathlib import Path
from PIL import Image
import sys
import os
def gravia_all():
  target_size = (224,224)
  dir_path = "bwh/*"
  max_size = len(glob.glob(dir_path))
  for i, name in enumerate(glob.glob(dir_path)):
    if i%10 == 0:
      print(i, max_size, name)
    save_name = name.split("/")[-1]
    type_name = name.split("/")[-2]
    try:
      img = Image.open(name)
    except OSError as e:
      continue
    w, h = img.size
    if w > h : 
      blank = Image.new('RGB', (w, w))
    if w <= h : 
      blank = Image.new('RGB', (h, h))
    try:
      blank.paste(img, (0, 0) )
    except OSError as e:
      continue
    blank = blank.resize( target_size )
    os.system("mkdir -p bwh_resize")
    blank.save("bwh_resize/{save_name}.mini.jpeg" \
      .format(save_name=save_name), "jpeg" ) 


def gravia_noisy():
  target_size = (224,224)
  dir_path = "./gravia-noisy-dataset/gravia/*/*"
  max_size = len(glob.glob(dir_path))
  for i, name in enumerate(glob.glob(dir_path)):
    if i%10 == 0:
      print(i, max_size, name)
    save_name = name.split("/")[-1]
    type_name = name.split("/")[-2]
    if Path("gravia-noisy-dataset/{type_name}/{save_name}.minify" \
        .format(type_name=type_name, save_name=save_name)).is_file(): 
      continue
    try:
      img = Image.open(name)
    except OSError as e:
      continue
    w, h = img.size
    if w > h : 
      blank = Image.new('RGB', (w, w))
    if w <= h : 
      blank = Image.new('RGB', (h, h))
    try:
      blank.paste(img, (0, 0) )
    except OSError as e:
      continue
    blank = blank.resize( target_size )
    os.system("mkdir -p gravia-noisy-dataset/{type_name}".format(type_name=type_name))
    blank.save("gravia-noisy-dataset/{type_name}/{save_name}.mini.jpeg" \
      .format(type_name=type_name, save_name=save_name), "jpeg" ) 

def cookpad():
  target_size = (224,224)
  dir_path = "../kotlin-phantomjs-selenium-jsoup-parser/dataset/*.jpg"
  max_size = len(glob.glob(dir_path))
  print("s")
  for i, name in enumerate(glob.glob(dir_path)):
    if i%10 == 0:
      print(i, max_size, name)
    save_name = name.split("/")[-1]
    if Path("cookpad/imgs/{save_name}.minify".format(save_name=save_name)).is_file(): 
      continue
    try:
      img = Image.open(name)
    except OSError as e:
      continue
    w, h = img.size
    if w > h : 
      blank = Image.new('RGB', (w, w))
    if w <= h : 
      blank = Image.new('RGB', (h, h))
    try:
      blank.paste(img, (0, 0) )
    except OSError as e:
      continue
    blank = blank.resize( target_size )
    blank.save("cookpad/imgs/{save_name}.mini.jpeg".format(save_name=save_name), "jpeg" ) 

def bwh():
  target_size = (224,224)
  dir_path = "../kotlin-phantomjs-selenium-jsoup-parser/bwh/*"
  max_size = len(glob.glob(dir_path))
  for i, name in enumerate(glob.glob(dir_path)):
    print(i, max_size, name)
    save_name = name.split("/")[-1]
    if Path("converted/{save_name}".format(save_name=save_name)).is_file(): 
      continue
    if ".png" in name:
      continue
    try:
      img = Image.open(name)
    except OSError as e: 
      continue
    w, h = img.size
    if w > h : 
      blank = Image.new('RGB', (w, w))
    if w <= h : 
      blank = Image.new('RGB', (h, h))
    blank.paste(img, (0, 0) )
    blank = blank.resize( target_size )
    print(blank.size)
    blank.save("converted/{save_name}".format(save_name=save_name) ) 
if __name__ == '__main__':
  if '--bwh' in sys.argv:
    bwh()
  if '--cookpad' in sys.argv:
    cookpad()
  if '--gravia_noisy' in sys.argv:
    gravia_noisy()
  if '--gravia_all' in sys.argv:
    gravia_all()
