import os
import math
import glob
import MeCab
import json
import pickle
import sys

def cookpad_index():
  m = MeCab.Tagger('-Ochasen')
  item_freq = {}
  for name in glob.glob("../kotlin-phantomjs-selenium-jsoup-parser/dataset/*.json"):
    with open(name) as f:
      o = json.loads(f.read())
    mat = " ".join(o["material"])
    terms = map(lambda x:x.split('\t')[0], filter(lambda x:'名詞' in x, m.parse(mat).strip().split('\n')) ) 
    for item in terms:
      if item_freq.get(item) is None: item_freq[item] = 0
      item_freq[item] += 1
  minify = { item:freq for item, freq in sorted(item_freq.items(), key=lambda x:x[1]*-1)[:2048] }

  item_index = {}
  for item, freq in minify.items():
    item_index[item] = len(item_index)
  open("cookpad/item_index.pkl", "wb").write(pickle.dumps(item_index))

def cookpad_minijson():
  m = MeCab.Tagger('-Ochasen')
  item_index = pickle.loads(open("cookpad/item_index.pkl", "rb").read())
  for name in glob.glob("../kotlin-phantomjs-selenium-jsoup-parser/dataset/*.json"):
    with open(name) as f:
      o = json.loads(f.read())
    mat = " ".join(o["material"])
    terms = map(lambda x:x.split('\t')[0], filter(lambda x:'名詞' in x, m.parse(mat).strip().split('\n')) ) 
    terms = map(lambda x:item_index[x], filter(lambda x:item_index.get(x), terms) )
    o["material"] = list(terms)

    imgUrl = o['imgUrl']
    del o['imgUrl']
    o['imgPath'] = "cookpad/imgs/{filename}.minify".format(filename=imgUrl.split('/')[-1])
    print(o)
    src = json.dumps(o)
    open('cookpad/json/{name}.minify'.format(name=name.split('/')[-1]), 'w').write(src)
if __name__ == '__main__':
  if '--cookpad_index' in sys.argv:
    cookpad_index()
  if '--cookpad_minijson' in sys.argv:
    cookpad_minijson()
