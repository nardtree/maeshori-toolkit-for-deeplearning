import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model, load_model
from keras.layers import Input, Activation, Dropout, Flatten, Dense, Reshape, merge
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization as BN
import numpy as np
import os
from PIL import Image
import glob 
import pickle
import sys
import msgpack
import msgpack_numpy as m; m.patch()
import numpy as np
import json
import re
import random
img_width, img_height = 150, 150
train_data_dir = './danbooru.imgs'
validation_data_dir = './imgs'
nb_train_samples = 2000
nb_validation_samples = 800
nb_epoch = 50
result_dir = 'results'

def loader(th = None):
  Xs = []
  Ys = [] 
  files = glob.glob('./gravia-noisy-dataset/*/*.mini.jpeg')
  random.shuffle(files)
  if '--mini' in sys.argv:
    files = files[:500]
  for gi, name in enumerate(files):
    if th is not None and gi > th:
      break
    last_name = name.split('/')[-1].split('_')[0]
    type_name = name.split('/')[-2] 
    print(name, type_name, file=sys.stderr)
    if type_name == 'negative':
      y = [0.]
    elif type_name == 'positive':
      y = [1.]
    img = Image.open('{name}'.format(name=name))
    img = img.convert('RGB')
    arr   = np.array(img)
    Ys.append( y   )
    Xs.append( arr )
  Xs = np.array(Xs)
  return Ys, Xs

#from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
def build_model():
  input_tensor = Input(shape=(224, 224, 3))
  model = VGG16(include_top=False, weights='imagenet', input_tensor=input_tensor)

  dense  = Flatten()( \
             Dense(2048, activation='relu')( \
               BN()( \
	              model.layers[-1].output ) ) )
  result = Activation('sigmoid')( \
	            Dense(1, activation="linear")(\
                 dense) )
  
  model = Model(input=model.input, output=result)
  for layer in model.layers[:11]:
    if 'BatchNormalization' in str(layer):
      ...
    else:
      layer.trainable = False
  model.compile(loss='binary_crossentropy', optimizer='adam')
  return model

def train():
  print('load lexical dataset...')
  Ys, Xs = loader()
  print('build model...')
  model = build_model()
  for i in range(100):
    model.fit(np.array(Xs), np.array(Ys), batch_size=16, nb_epoch=1 )
    if i%1 == 0:
      model.save('models/model%05d.model'%i)

def eval():
  model = build_model()
  model = load_model(sorted(glob.glob('models/*.model'))[-1]) 
  Ys, Xs = loader(th=10)
  for i in range(len(Xs)):
    result = model.predict(np.array([Xs[i]]) )
    for i, w in enumerate(result.tolist()[0]):
      print(i, w)

def classify():
  os.system("mkdir ok")
  os.system("mkdir ng")
  model = build_model()
  model = load_model(sorted(glob.glob('models/*.model'))[-1]) 
  files = glob.glob("bwh_resize/*")
  random.shuffle(files)
  for gi, name in enumerate(files):
    try:
      img = Image.open('{name}'.format(name=name))
    except FileNotFoundError as e:
      continue
    img = [np.array(img.convert('RGB'))]
    if not os.path.exists(name):
      continue
    result = model.predict(np.array(img) )
    result = result.tolist()[0]
    result = { i:w for i,w in enumerate(result)}
    for i,w in sorted(result.items(), key=lambda x:x[1]*-1):
      if w > 0.65:
        os.system("mv {name} ok/".format(name=name))
      else:
        os.system("mv {name} ng/".format(name=name))
      print(gi, name, w, file=sys.stderr)
if __name__ == '__main__':
  if '--train' in sys.argv:
    train()
  if '--eval' in sys.argv:
    eval()
  if '--classify' in sys.argv:
    print("a")
    classify()
